*****************About Game***************

What is Nim-Sum Game?
The traditional game of Nim is played with a number of coins arranged in heaps: the number of coins and heaps is up to you. There are two players. When it's a player's move he or she can take any number of coins from a single heap. They have to take at least one coin, though, and they can't take coins from more than one heap. The winner is the player who makes the last move, so there are no coins left after that move. 
Further details you can find here http://en.wikipedia.org/wiki/Nim

****************How to Play it**************

Pre-requisite:
1. Install mysql having root password as "root" 
2. Create database "gt"
3. Create table players(email_id varchar(100),name varchar(50),state int)


Its very simple, have to do following steps:
1. Download apache-tomcat-7.0.55.tar.gz, gt.war,mysql-connector-java-5.1.6-bin.jar
2. Extract apache-tomcat-7.0.55.tar.gz and in lib folder copy mysql-connector-..
3. Copy gt.war in webapps folder of apache-tomcat-7.0.55
4. Go to bin folder and run ./startup.sh 
That's it, now you can play from browser as http://localhost:8080/GT/



****************How to Modify it*************
1. GT folder is eclipse project
2. Its src files contains all the Servlet files [.java]
	-ConnectionManager : Database connectivity
	-Nim		   : Logic of game [computer levels]
	-Login/Logout	   : While login store in DB and Logout if he was playing notify other player
	-Welcome	   : Check if 2 players can play wiht each other or have to play with computer
	-Game		   : For each game we create its object, and use it
	-Play		   : It is called after each move of the player
3. In webContent, we have all the jsp files [.jsp]