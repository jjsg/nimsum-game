<!DOCTYPE html>
<html>
<head>
<title>NimSum</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<link rel="icon" href="css/images/favicon.gif" type="image/x-icon" />
<!--[if IE 6]><link rel="stylesheet" href="css/ie6-style.css" type="text/css" media="all" /><![endif]-->
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/fns.js" type="text/javascript"></script>
</head>
<body>
<%
response.setHeader("Cache-Control","no-cache");
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);%>
<!-- Page -->
<div id="page" class="shell">
  <!-- Header -->
  <div id="header">
    <!-- Top Navigation -->
    <div id="top-nav">
      <ul>
            <li class="home"><a href="index.jsp">home</a></li><!--------------------TODO session clear on load------>
      </ul>
    </div>
    <!-- / Top Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Logo -->
    <div id="logo">
     
    </div>
    <!-- / Logo -->
    <!-- Main Navigation -->
    <div id="main-nav">
      <div class="bg-right">
        <div class="bg-left">
          
        </div>
      </div>
    </div>
    <!-- / Main Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Sort Navigation -->
    <div id="sort-nav">
      <div class="bg-right">
        <div class="bg-left">
          <div class="cl">&nbsp;</div>
          <ul>
                   
             <%  if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
%>
 <li><a href="index.jsp">Home</a><span class="sep">&nbsp;</span></li>
    <li><a href="login.jsp">Log In</a><span class="sep">&nbsp;</span></li>
<% 	 
}
else
{
%> 
 <li><a href="welcome.jsp">Welcome</a><span class="sep">&nbsp;</span></li>
   <li><a  onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a><span class="sep">&nbsp;</span></li>
<% 	 
	
}
 %>
            <li><a href="info.jsp">Know Nim!</a><span class="sep">&nbsp;</span></li>
      
            <li><a href="tutorial.jsp">Tutorial</a><span class="sep">&nbsp;</span></li>
           <li><a href="https://www.facebook.com/NimSumGame" target="_blank">Follow us on FB</a><span class="sep">&nbsp;</span></li>
           <li><a href="aboutus.jsp">Know Us!</a><span class="sep">&nbsp;</span></li>
          </ul>
          <div class="cl">&nbsp;</div>
        </div>
      </div>
    </div>
    <!-- / Sort Navigation -->
  </div>
  <!-- / Header -->

  <!-- Main -->
  <div id="main">
    <div id="main-bot">
      <div class="cl">&nbsp;</div>
      <!-- Content -->
      <div id="content">
        <div class="block">
          <div class="block-bot">
            
        </div>
            <div style="background: #C9CCD3;border: solid 10px #313131; border-radius: 5px;">
                
                <br/><br/>
    <p style="padding: 5px; color:  #e05f1d;font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Happy to connect with u :) 
        <br/>
       
                </p>
                <p style="padding: 5px; color: #e05f1d;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NimSum Game Project :: Game Theory [CSE593] :: Monsoon 2014 :: IIITH</p>
                <br/><br/>
                
                  
        <ul>
            
            
            <ul style="color: black;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/lovlean.arora?fref=ts" target="_blank" style="color: black;"><img width=20px height=20px src="css/images/fb.jpeg"/>&nbsp;&nbsp;Lovlean Arora : PG2 - CSE </a></ul>
            <br/>
             <ul style="color: black;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/dexters1989?fref=ts" target="_blank" style="color: black;"><img width=20px height=20px src="css/images/fb.jpeg"/>&nbsp;&nbsp;Harsha Vardhan : PG2 - CSIS </a></ul>
            <br/> 
            <ul style="color: black;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/raj.mehta.92102" target="_blank" style="color: black;"><img width=20px height=20px src="css/images/fb.jpeg"/>&nbsp;&nbsp;Raj Mehta : PG2 - CSE </a></ul>
            <br/> 
           <ul style="color: black;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/profile.php?id=100001473314455&fref=ts" target="_blank" style="color: black;"><img width=20px height=20px src="css/images/fb.jpeg"/>&nbsp;&nbsp;Ritika Sharma : PG2 - CSE </a></ul>
                
    </ul>
                <br/><br/><br/>
                
                <p style="padding: 5px; color: #e05f1d;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Under the guidance of Ganesh Iyer, mentored by Ayush Datta</p>
                <br/><br/>

</div>

      </div>


     
      </div>
    </div>

    <!-- / Content -->
   
        
    
    <div id="sidebar">
     

      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt">
              <h3>Know Nim!</h3>
            </div>
          </div>
          <div class="image-articles articles">
            <div class="cl">&nbsp;</div>
            <div class="article">
              <div class="cl">&nbsp;</div>
              
              <div class="cnt" style="float: left;">
              
               <p style="text-align: justify;">Your winning chance depends on the roll of a die or the cards you've been dealt. But there are other games that are only about strategy: if you play cleverly, you're guaranteed to win...</p>
              </div>
              <div class="cl">&nbsp;</div>
            </div>
            <div class="article">
             
            <a href="info.jsp" class="view-all">view more...</a>
            <div class="cl">&nbsp;</div>
          </div>
        </div>
      </div>
      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt">
              <h3>Tutorial</h3>
            </div>
          </div>
          <div class="image-articles articles">
            
           
            <div class="cl">&nbsp;</div>
            <div class="article">
              <div class="cl">&nbsp;</div>
              
              <div class="cnt" style="float: left;">
             <div>
<iframe width="210" height="250" src="http://www.youtube.com/embed/cafwUNrg73M" frameborder="0" allowfullscreen></iframe>
</div>

               
              </div>

              <div class="cl">&nbsp;</div>
            </div>
         
            <div class="cl">&nbsp;</div>
          </div>
        </div>
      </div>
      
    </div></div></div>
    <!-- / Sidebar -->
    <div class="cl">&nbsp;</div>
          
    <!-- Footer -->
    <div id="footer">
      <div class="navs">
        <div class="navs-bot">
          <div class="cl">&nbsp;</div>
          <ul>
               <%  if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
%>
 <li><a href="index.jsp">Home</a><span class="sep">&nbsp;</span></li>
    <li><a href="login.jsp">Log In</a><span class="sep">&nbsp;</span></li>
<% 	 
}
else
{
%> 
 <li><a href="welcome.jsp">Welcome</a><span class="sep">&nbsp;</span></li>
   <li><a  onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a><span class="sep">&nbsp;</span></li>
<% 	 
	
}
 %>            <li><a href="info.jsp">Know Nim!</a><span class="sep">&nbsp;</span></li>
      
            <li><a href="tutorial.jsp">Tutorial</a><span class="sep">&nbsp;</span></li>
           <li><a href="https://www.facebook.com/NimSumGame" target="_blank">Follow us on FB</a><span class="sep">&nbsp;</span></li>
           <li><a href="aboutus.jsp">Know Us!</a><span class="sep">&nbsp;</span></li>
          </ul>
         
          <div class="cl">&nbsp;</div>
        </div>
      </div>
      <p class="copy" style="color: #e05f1d;">&copy;Designed By: NimSum PG2 Team</p>
    </div>
    <!-- / Footer -->
  </div>
</div>
<!-- / Main -->
</div>
<!-- / Page -->

</body>
</html>

