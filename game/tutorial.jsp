<!DOCTYPE html>
<html>
<head>
<title>NimSum</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<link rel="icon" href="css/images/favicon.gif" type="image/x-icon" />
<!--[if IE 6]><link rel="stylesheet" href="css/ie6-style.css" type="text/css" media="all" /><![endif]-->
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/fns.js" type="text/javascript"></script>
</head>
<body>
<%
response.setHeader("Cache-Control","no-cache");
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);%>
<!-- Page -->
<div id="page" class="shell">
  <!-- Header -->
  <div id="header">
    <!-- Top Navigation -->
    <div id="top-nav">
      <ul>
        <li class="home"><a href="index.jsp">home</a></li>
      
      
      </ul>
    </div>
    <!-- / Top Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Logo -->
    <div id="logo">
     
    </div>
    <!-- / Logo -->
    <!-- Main Navigation -->
    <div id="main-nav">
      <div class="bg-right">
        <div class="bg-left">
         
        </div>
      </div>
    </div>
    <!-- / Main Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Sort Navigation -->
    <div id="sort-nav">
      <div class="bg-right">
        <div class="bg-left">
          <div class="cl">&nbsp;</div>
          <ul>
                    <%  if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
%>
 <li><a href="index.jsp">Home</a><span class="sep">&nbsp;</span></li>
    <li><a href="login.jsp">Log In</a><span class="sep">&nbsp;</span></li>
<% 	 
}
else
{
%> 
 <li><a href="welcome.jsp">Welcome</a><span class="sep">&nbsp;</span></li>
   <li><a  onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a><span class="sep">&nbsp;</span></li>
<% 	 
	
}
 %>            <li><a href="info.jsp">Know Nim!</a><span class="sep">&nbsp;</span></li>
      
            <li><a href="tutorial.jsp">Tutorial</a><span class="sep">&nbsp;</span></li>
           <li><a href="https://www.facebook.com/NimSumGame" target="_blank">Follow us on FB</a><span class="sep">&nbsp;</span></li>
           <li><a href="aboutus.jsp">Know Us!</a><span class="sep">&nbsp;</span></li>
          </ul>
          <div class="cl">&nbsp;</div>
        </div>
      </div>
    </div>
    <!-- / Sort Navigation -->
  </div>
  <!-- / Header -->

  <!-- Main -->
  <div id="aaa">
  <div style="padding:20px;">
<h1 style="text-align: center; color: #e05f1d; padding: 20px;">Lets Learn...!!!!</h1>
<iframe width="900" height="600" src="http://www.youtube.com/embed/cafwUNrg73M" frameborder="0" allowfullscreen></iframe>
</div>

   </div>
    <div class="cl">&nbsp;</div>
    <!-- Footer -->
    <div id="footer">
      <div class="navs">
        <div class="navs-bot">
          <div class="cl">&nbsp;</div>
          <ul>
                <%  if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
%>
 <li><a href="index.jsp">Home</a><span class="sep">&nbsp;</span></li>
    <li><a href="login.jsp">Log In</a><span class="sep">&nbsp;</span></li>
<% 	 
}
else
{
%> 
 <li><a href="welcome.jsp">Welcome</a><span class="sep">&nbsp;</span></li>
   <li><a  onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a><span class="sep">&nbsp;</span></li>
<% 	 
	
}
 %>            <li><a href="info.jsp">Know Nim!</a><span class="sep">&nbsp;</span></li>
      
            <li><a href="tutorial.jsp">Tutorial</a><span class="sep">&nbsp;</span></li>
           <li><a href="https://www.facebook.com/NimSumGame" target="_blank">Follow us on FB</a><span class="sep">&nbsp;</span></li>
           <li><a href="aboutus.jsp">Know Us!</a><span class="sep">&nbsp;</span></li>
          </ul>
         
          <div class="cl">&nbsp;</div>
        </div>
      </div>
      <p class="copy" style="color: #e05f1d;">&copy;Designed By: NimSum PG2 Team</p>
    </div>
    <!-- / Footer -->
  </div>
</div>
<!-- / Main -->
</div>
<!-- / Page -->

</body>
</html>
