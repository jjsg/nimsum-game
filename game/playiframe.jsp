<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<base target="_parent" />
<title>Insert title here</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<script>
       var x,y;
      // function testing()
      // {
    	//  var ft=
    	//  alert(ft);
    	//  if("true"==ft)
    	//	  {
    	//	  submitf();
    	//	  }
       //}
function f1()
{
var audioo=document.createElement("audio");
audioo.src="css/audio/Your turn.ogg";
audioo.style.display="none";
audioo.type="audio/ogg"
audioo.autoplay="true";
}
function f2()
{
var audioo=document.createElement("audio");
audioo.src="css/audio/Wait.ogg";
audioo.style.display="none";
audioo.type="audio/ogg"
audioo.autoplay="true";
}
function forfeitf()
{
	document.getElementById("move").value="0-0";
	submitf();
	}
        function submitf()
        {
        	f2();
        	document.getElementById("play_id").submit();
        	document.getElementById("playdivenable").style.pointerEvents="none";
        	document.getElementById("cmove").innerHTML="Waiting for other person to play his turn";
        	document.getElementById("cmove").style.paddingLeft="200px";
        	document.getElementById("cmove").style.color="red";
        	for(i=1;i<=x;i++)
        	{
        	
        	for(j=1;j<=y;j++)
             {
        		tt=i+"_"+j;
        		
        		
        		document.getElementById(tt).style.borderColor="red";
        		  document.getElementById(tt).title="You can't pick from this heap";
             }
        		
             }
        	}
        
        function image_selected(id)
        {
          
        	document.getElementById(id).style.pointerEvents="none";
        	document.getElementById("submitbutton").disabled=false;
        	document.getElementById("undobutton").disabled=false;
          document.getElementById(id).style.borderColor="green";
          document.getElementById(id).title="Already Picked";
          
          var arr = id.split("_");
          var row=arr[0];
          var col=arr[1];
          var initial=document.getElementById("move").value;
          if (initial=="")
        	  {
        	  document.getElementById("move").value=row+"-"+col;
        	  
        	  }
          else
        	  {
        	  document.getElementById("move").value=initial+","+col;
        	  }
        	  
          
for(i=1;i<=x;i++)
	{
	if(i==parseInt(row))
		{
		continue;
		}
	else
		{
	for(j=1;j<=y;j++)
     {
		tt=i+"_"+j;
		
		document.getElementById(tt).style.pointerEvents="none";
		document.getElementById(tt).innerHTML="X";
		document.getElementById(tt).style.borderColor="red";
		  document.getElementById(tt).title="You can't pick from this heap";
     }
		
     }
	}
}
 
    
        function image_customize(id)
        {	
        	for(i=1;i<=x;i++)
        	{
        	
        	for(j=1;j<=y;j++)
             {
        		tt=i+"_"+j;
        		
        	
        		document.getElementById(tt).style.backgroundImage = "url('css/images/p"+id+".jpg')";
        		document.getElementById("imageselected").value=id;
        		
        		 
             }
        		
             }
        	document.getElementById("customization").style.display="none";
        }
        function show()
        {
			document.getElementById("customization").style.display="block";
        } 
       
        </script>
</head>
<body>

   <%
   response.setHeader("Cache-Control","no-cache");
   response.setHeader("Cache-Control","no-store");
   response.setHeader("Pragma","no-cache");
   response.setDateHeader ("Expires", 0);
String pointerEvents="none";
String inner="Waiting for other person to play his turn";
String paddingLeft="200px";
	String borderColor="red";
	String color="red";
		String title="You can't pick from this heap";
		String logoutdisplay="none";
		String autoplay="false";
if((((String)session.getAttribute("myturn")).equals("true")))
{
	 pointerEvents="auto";
	  inner="Play your move...";
	 paddingLeft="350px";
		 borderColor="orange";
		 color="green";
			 title="Click to Pick";
			 logoutdisplay="block";
			out.println("<script> f1();</script>");
}


%>

<div>
<%
 int noOfImages=6;
String defaultimage=(String)session.getAttribute("imageselected");

%>
<button id="customize" onclick="show()">Customize your object look..!!!!</button>

<br>
<br>
<div id="customization" style="display: none" >
<%

for (int i=1;i<=noOfImages;i++)
{
	%>
	<button title="Click it to select this look of the objects" style="width:83px;height: 83px;background-image: url('css/images/p<%=i%>.jpg');" id="<%=i%>" onclick="image_customize(this.id)"></button>
<%		
}
%>

</div>
<audio  style="display:none" controls id="yourturnaudio">
  <source src="css/audio/Your turn.ogg" type="audio/ogg">
 <source src="css/audio/Your turn.m4a" type="audio/mp4">



</div>
<div id="playdivenable" style="pointer-events: <%= pointerEvents%>">
<label style="float:right;font-size:15px;">You can only quit the game when it is your turn.&nbsp;&nbsp;
<a  onclick="return confirm('Are you sure you want to Quit Playing')" style='text-decoration:underline' href='logout?playing=true'>Quit Playing</a></label>
<br>
<br>
        <script>
       
      
        </script>
        <label id="cmove" style="padding-left:<%= paddingLeft%>; font-size: 30px; color: <%=color%>"><%=inner%></label><br><br>
        <table>
        <%
        int x,y;
        x=(Integer)session.getAttribute("grid_size_x");
        y=(Integer)session.getAttribute("grid_size_y");
      
        
        int pattern[][]=(int[][])session.getAttribute("array");
        out.println("<script>x="+x+";y="+y+";</script>");
        for(int i=1;i<=x;i++)
        {
        %>
        <tr>
        <%
        	for(int j=1;j<=y;j++)
        	{
       	%>
       	<td style=" width: 83px; height: 83px; padding-right: 2px;padding-bottom: 5px;">
       	<% if (pattern[i][j]==1)
       		{
       		%>
       		
       		<button title=<%=title %>  style="font-size: 80px; color: red;border: solid 2px <%= borderColor %>; width:83px;height: 83px;background-image: url('css/images/p<%=defaultimage%>.jpg');" id="<%=i%>_<%=j%>" onclick="image_selected(this.id)"></button>
       		<%
       		}
       	else
       	{
       		%>
       		<button style="display:none; border: solid 2px orange; width:83px;height: 83px;background-image: url('css/images/p<%=defaultimage%>.jpg');" id="<%=i%>_<%=j%>" onclick="image_selected(this.id)"></button>
       		
       		<%
       	}
       		%>
       	</td>
       	<% 
        	}
        %>
       	</tr>
       	<% 
        }   
         %>
        </table>
        <form id="play_id" action="play" method="POST" >
       
     
       <input type="text" hidden="hidden" name="move" value="" id="move"/>
       <input type="text" hidden="hidden" name="imageselected" value="<%=defaultimage %>" id="imageselected"/>
          <button style="margin:10px; padding:5px;" type="submit" class="vsclass" id="submitbutton" onclick="submitf()" disabled>Submit Your Move</button>
          <button style="margin:10px; padding:5px;" type="button" class="vsclass" id="undobutton" disabled="true" onclick="undof()">Reset</button>
          <label style="color:red">* Select the move to enable the previous buttons</label><br>
          <button style="margin:10px; padding:5px;" type="button" class="vsclass" id="forfeitbutton" onclick="forfeitf()">Forfeit</button>
         
          </form>
        
          <script type="text/javascript">function undof() {location.reload(); document.getElementById("move").value=""; document.getElementById("imageselected").value="";}</script>
        </div>  
<%
if("true".equals((String)session.getAttribute("firsttime")))
{
	out.println("<script>submitf();</script>");
}
%>

        

</body>
</html>