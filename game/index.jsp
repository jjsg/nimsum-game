<!DOCTYPE html>
<html>
<head>
<title>NimSum</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<link rel="icon" href="css/images/favicon.gif" type="image/x-icon" />
<!--[if IE 6]><link rel="stylesheet" href="css/ie6-style.css" type="text/css" media="all" /><![endif]-->
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/fns.js" type="text/javascript"></script>
</head> 
<body>
<!-- Page -->
<div id="page" class="shell">
  <!-- Header -->
  <div id="header">
    <!-- Top Navigation -->
    <div id="top-nav">
      <ul>
        <li class="home"><a href="index.jsp">home</a></li>
      </ul>
    </div>
    <!-- / Top Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Logo -->
    <div id="logo">
     
    </div>
    <!-- / Logo -->
    <!-- Main Navigation -->
    <div id="main-nav">
      <div class="bg-right">
        <div class="bg-left">
         
        </div>
      </div>
    </div>
    <!-- / Main Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Sort Navigation -->
    <div id="sort-nav">
      <div class="bg-right">
        <div class="bg-left">
          <div class="cl">&nbsp;</div>
          <ul>
                    <%  if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
%>
 <li><a href="index.jsp">Home</a><span class="sep">&nbsp;</span></li>
    <li><a href="login.jsp">Log In</a><span class="sep">&nbsp;</span></li>
<% 	 
}
else
{
%> 
 <li><a href="welcome.jsp">Welcome</a><span class="sep">&nbsp;</span></li>
   <li><a  onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a><span class="sep">&nbsp;</span></li>
<% 	 
	
}
 %>     
            <li><a href="info.jsp">Know Nim!</a><span class="sep">&nbsp;</span></li>
           <li><a href="tutorial.jsp">Tutorial</a><span class="sep">&nbsp;</span></li>
           <li><a href="https://www.facebook.com/NimSumGame" target="_blank">Follow us on FB</a><span class="sep">&nbsp;</span></li>
           <li><a href="aboutus.jsp">Know Us!</a><span class="sep">&nbsp;</span></li>
          </ul>
          <div class="cl">&nbsp;</div>
        </div>
      </div>
    </div>
    <!-- / Sort Navigation -->
  </div>
  <!-- / Header -->

  <!-- Main -->
  <div id="main">
    <div id="main-bot">
      <div class="cl">&nbsp;</div>
      <!-- Content -->
      <div id="content">
        <div class="block">
          <div class="block-bot">
            <div class="block-cnt">
              <div id="slider">
                <div class="buttons"> <span class="prev">prev</span> <span class="next">next</span> </div>
                <div class="holder">
                  <div class="frame">&nbsp;</div>
                  <div class="content">
                    <ul>
                    <li class="fragment">
                      <div class="image">
                      <img src="css/images/img1.gif" alt="" /> 
                      </div>
                  <div class="cnt">
                    <div class="cl">&nbsp;</div>
                    <div class="side-a">
                      <h4 style="color:#e05f1d">NimSum Logo</h4>
                     
                    </div>
                    <div class="side-b">
                      <p  style="color:black">Play Game to explore more...</p>
                    </div>
                    <div class="cl">&nbsp;</div>
                  </div>
                  </li>
                    <li class="fragment">
                      <div class="image">
                      <img src="css/images/img2.png" alt="" /> 
                      </div>
                  <div class="cnt">
                    <div class="cl">&nbsp;</div>
                    <div class="side-a">
                      <h4 style="color:#e05f1d">Pairing Module</h4>
                     
                    </div>
                    <div class="side-b">
                      <p style="color:black">Play Game to explore more...</p>
                    </div>
                    <div class="cl">&nbsp;</div>
                  </div>
                  </li>
                    <li class="fragment">
                      <div class="image">
                      <img src="css/images/img3.png" alt="" /> 
                      </div>
                  <div class="cnt">
                    <div class="cl">&nbsp;</div>
                    <div class="side-a">
                      <h4 style="color:#e05f1d">Play Module</h4>
                     
                    </div>
                    <div class="side-b">
                      <p style="color:black">Play Game to explore more...</p>
                    </div>
                    <div class="cl">&nbsp;</div>
                  </div>
                  </li>
                    <li class="fragment">
                      <div class="image">
                      <img src="css/images/img4.png" alt="" /> 
                      </div>
                  <div class="cnt">
                    <div class="cl">&nbsp;</div>
                    <div class="side-a">
                      <h4 style="color:#e05f1d">Wait Module</h4>
                     
                    </div>
                    <div class="side-b">
                      <p style="color:black">Play Game to explore more...</p>
                    </div>
                    <div class="cl">&nbsp;</div>
                  </div>
                  </li>
                      <li class="fragment">
                      <div class="image">
                      <img src="css/images/img5.png" alt="" /> 
                      </div>
                  <div class="cnt">
                    <div class="cl">&nbsp;</div>
                    <div class="side-a">
                      <h4 style="color:#e05f1d">Result Module</h4>
                     
                    </div>
                    <div class="side-b">
                      <p style="color:black">Play Game to explore more...</p>
                    </div>
                    <div class="cl">&nbsp;</div>
                  </div>
                  </li>
                    <li class="fragment">
                      <div class="image">
                      <img src="css/images/img10.jpg" alt="" /> 
                      </div>
                  <div class="cnt">
                    <div class="cl">&nbsp;</div>
                    <div class="side-a">
                      <h4 style="color:#e05f1d">Customized Object</h4>
                     
                    </div>
                    <div class="side-b">
                      <p style="color:black">Play Game to explore more...</p>
                    </div>
                    <div class="cl">&nbsp;</div>
                  </div>
                  </li>
                  </ul>
                </div><!-- content-->
              </div><!-- holder -->
            </div><!-- slider -->

          </div><!-- block-cnt -->

        </div><!-- block-bot -->

      </div><!-- block -->
<div style="background: #949495;border: solid 10px #313131; border-radius: 5px;"><p style="padding: 5px; color: black;">
<img width=30px height=30px src="css/images/fb.jpeg"/>&nbsp;&nbsp;
<a href="https://www.facebook.com/NimSumGame" style="font-size:20px;">Follow us on FB</a>
<br/><br>
FB has made space/impact in/on every sphere of our life. Nowadays, What's life without FB?  <br><br>
NimSum also has its page on FB, where you can know more about it, post your suggestions/queries/comment/etc....<br><br>
Be active and Keep Having unlimited fun :) Stay tuned..!!! <br><br>

</div>

     
     
    </div>
    <!-- / Content -->
  
    <div id="sidebar">
     
     
      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt">
              <h3>Know Nim!</h3>
            </div>
          </div>
          <div class="image-articles articles">
            <div class="cl">&nbsp;</div>
            <div class="article">
              <div class="cl">&nbsp;</div>
              
              <div class="cnt" style="float: left;">
              
                <p style="text-align: justify;">Your winning chance depends on the roll of a die or the cards you've been dealt. But there are other games that are only about strategy: if you play cleverly, you're guaranteed to win...</p>
              </div>
              <div class="cl">&nbsp;</div>
            </div>
            <div class="article">
             
            <a href="info.jsp" class="view-all">view more...</a>
            <div class="cl">&nbsp;</div>
          </div>
        </div><!-- image-article -->
      </div><!-- block bot -->

</div><!-- block added -->
      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt">
              <h3>Tutorial</h3>
            </div>
          </div>
          <div class="image-articles articles">
            
           
            <div class="cl">&nbsp;</div>
            <div class="article">
              <div class="cl">&nbsp;</div>
              
              <div class="cnt" style="float: left;">
             <div>
<iframe width="210" height="250" src="http://www.youtube.com/embed/cafwUNrg73M" frameborder="0" allowfullscreen></iframe>
</div>

               
              </div>
              <div class="cl">&nbsp;</div>
            </div>
         
            <div class="cl">&nbsp;</div>
          </div><!-- image article -->
        </div><!-- block-bot -->
      </div><!-- block -->
      

   
    
 </div>
    <!-- / Sidebar -->
    
    <div class="cl">&nbsp;</div>
    <!-- Footer -->
    <div id="footer">
      <div class="navs">
        <div class="navs-bot">
          <div class="cl">&nbsp;</div>
          <ul>
             <%  if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
%>
 <li><a href="index.jsp">Home</a><span class="sep">&nbsp;</span></li>
    <li><a href="login.jsp">Log In</a><span class="sep">&nbsp;</span></li>
<% 	 
}
else
{
%> 
 <li><a href="welcome.jsp">Welcome</a><span class="sep">&nbsp;</span></li>
   <li><a  onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a><span class="sep">&nbsp;</span></li>
<% 	 
	
}
 %>
            <li><a href="info.jsp">Know Nim!</a><span class="sep">&nbsp;</span></li>
      
            <li><a href="tutorial.jsp">Tutorial</a><span class="sep">&nbsp;</span></li>
           <li><a href="https://www.facebook.com/NimSumGame" target="_blank">Follow us on FB</a><span class="sep">&nbsp;</span></li>
           <li><a href="aboutus.jsp">Know Us!</a><span class="sep">&nbsp;</span></li>
          </ul>
         
          <div class="cl">&nbsp;</div>
        </div>
      </div>
      <p class="copy" style="color: #e05f1d;">&copy;Designed By: NimSum PG2 Team</p>
    </div>
    <!-- / Footer -->

</div>
</div>
<!-- / Main -->
</div>
<!-- / Page -->

</body>
</html>
