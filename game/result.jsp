<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.Map"%>
<!DOCTYPE html>

<head>
<title>NimSum</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<link rel="icon" href="css/images/favicon.gif" type="image/x-icon" />
<!--[if IE 6]><link rel="stylesheet" href="css/ie6-style.css" type="text/css" media="all" /><![endif]-->
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/fns.js" type="text/javascript"></script>
 <script type="text/javascript">
      function f2()
      {
      var audioo=document.createElement("audio");
      audioo.src="css/audio/Won.ogg";
      audioo.style.display="none";
      audioo.type="audio/ogg"
      audioo.autoplay="true";
      }
      function f3()
      {
      var audioo=document.createElement("audio");
      audioo.src="css/audio/Lost.ogg";
      audioo.style.display="none";
      audioo.type="audio/ogg"
      audioo.autoplay="true";
      }
      </script>
</head>
<body>

<%
response.setHeader("Cache-Control","no-cache");
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);
if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
//	out.println("hererrrrrrrr");
	response.setStatus(response.SC_MOVED_TEMPORARILY);
	response.setHeader("Location", "/GT/login.jsp"); 
}
else
{	
%>
<!-- Page -->
<div id="page" class="shell">
  <!-- Header -->
  <div id="header">
    <!-- Top Navigation -->
    <div id="top-nav">
       
      
           
    </div>
    <!-- / Top Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Logo -->
    <div id="logo">
     
    </div>
    <!-- / Logo -->
    <!-- Main Navigation -->
    <div id="main-nav">
      <div class="bg-right">
        <div class="bg-left">
          

        </div>
      </div>
    </div>
    <!-- / Main Navigation -->
    <div class="cl">&nbsp;</div>

  </div>
  <!-- / Header -->

  <!-- Main -->
  <div id="main">
    <div id="main-bot">
      <div id="playdivenable" style="width: 900px; border: solid 5px #e05f1d; border-radius:3px 3px; background:#c9ccd3">
   <%
int originalPattern[][]=(int[][])session.getAttribute("originalpattern");
String message=(String)session.getAttribute("message");
Map<Integer,String> playMap=(Map<Integer,String>)session.getAttribute("result");
String first=(String)session.getAttribute("first");
String second=(String)session.getAttribute("second");
int grid_size_x=(Integer)session.getAttribute("grid_size_x");
int grid_size_y=(Integer)session.getAttribute("grid_size_y");



%>


<div id="playdivenable" >
<div  style="float:right;font-size:20px;color:black"><a title="click to play again" href="/GT/playagain.jsp">Click to Play Again</a>&nbsp;&nbsp;&nbsp;<a  onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a></div>
<h1 style="padding:15px; color: black">And the result is.....</h1>
    <label style="padding: 15px;font-size:20px; color: #e05f1d;" id="messageanimate"><%=message%></label>
    <%
    if("Congratzzz!!!! You Won :)".equals(message))
    out.println("<script>f2();</script>");
    else
    	out.println("<script>f3();</script>");	
    %>
    <br><br><br>
    <label style="font-size:15px;padding:10px; color:black;" id="historyanimate">History of how your game moved..</label>
    <div id="movesanimate"  style="font-size:15px;padding:10px; color:black;">
    <br>
   <label style="font-size:15px;padding:10px; color:#e05f1d;">Initial Heaps: <%=grid_size_x %></label>
    <br>
     <label style="font-size:15px;padding:10px; color:#e05f1d;">Original Pattern(represented as binary):</label>
    <br>
    <p style="padding:10px;">
    <%
    for(int i=1;i<=grid_size_x;i++)
    {
    	out.print("Heap"+i+": ");
    	for(int j=1;j<=grid_size_y;j++)
    	{
    		out.print(originalPattern[i][j]+" ");		
    	}
    	%>
    	<br>
    <%
    	
    }
    %>
    </p>
    <br><br>
    <label style="font-size:20px;padding:10px; color:#e05f1d;">Game as it progressed:</label>
    
    <table id="resulttable">
    <th style="font-size:15px;padding:2px; color:black;">
    Player
    </th>
    <th style="font-size:15px;padding:2px; color:black;">
    HeapNo-Object1,Object2...
    </th>
    
    <%boolean isFirst=true;%>
    <%for(Map.Entry<Integer,String> me: playMap.entrySet())
    {
    %>
    <tr>  
    <td style="font-size:15px;padding:2px; color:black;">
    <%
    if(isFirst)
    {
    	out.print(first);
    	isFirst=false;
    }
    else
    {
    	out.print(second);
    	isFirst=true;
    }
    %>
    </td>
    <td style="font-size:15px;padding:2px; color:black;">
    <%=me.getValue() %>
    </td>
    </tr>
    <%
    }
    %>
    </table>
       
</div>  

        
</div>
		</div>
    
     
	</div>    
	<!-- main-bot -->
    
    <div class="cl">&nbsp;</div>
    <!-- Footer -->
    <div id="footer">
      <div class="navs">
        <div class="navs-bot">
          <div class="cl">&nbsp;</div>
         
         
          <div class="cl">&nbsp;</div>
        </div>
      </div>
      <p class="copy" style="color: #e05f1d;">&copy;Designed By: NimSum PG2 Team</p>
    </div>
    <!-- / Footer -->
  </div>

<!-- / Main -->
</div>
<!-- / Page -->

<%
//while(((String)session.getAttribute("myturn")).equals("false"));

/*if((((String)session.getAttribute("myturn")).equals("win")))
{
	response.setStatus(response.SC_MOVED_TEMPORARILY);
	response.setHeader("Location", "/GT/win.jsp"); 
}
else if((((String)session.getAttribute("myturn")).equals("true")))
{
	out.println("<script>document.getElementById(\"playdivenable\").style.pointerEvents=\"auto\" </script>");
}
*/
}
%>
</body>
</html>
    