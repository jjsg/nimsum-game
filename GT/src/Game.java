import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpSession;


public class Game  {

	int move_id;
	Boolean gameover=false;
	String current_player,opponent_player,first,second,winner;
	Map<Integer,String> move_details;
	int grid_size_x;
	int grid_size_y;
	
	int nonzerosCount=0;
	int array[][]=new int[15][15];
	int original[][]=new int[15][15];
	String player1_email,player2_email;
	Map<String, HttpSession> session_details;
	
	public Game(String p1_email,String p2_email,int sizex,int sizey) {
		
		move_id=0;
		move_details=new HashMap<Integer, String>();
		session_details= new HashMap<String, HttpSession>();
		player1_email=p1_email;
		player2_email=p2_email;
		if(sizex==-1)
		{
			grid_size_x=new Random().nextInt(8)+3;
			grid_size_y=new Random().nextInt(8)+3;
		}
		else
		{
			grid_size_x=sizex;
			grid_size_y=sizey;
		}
		
		while(nonzerosCount<=grid_size_x)
		{
			nonzerosCount=0;
			for(int i=1;i<=grid_size_x;i++)
			{
				for(int j=1;j<=grid_size_y;j++)
				{
					array[i][j]=new Random().nextInt(2);
					original[i][j]=array[i][j];
					if(array[i][j]!=0)
						nonzerosCount++;
				}
			}
		}
		/*nonzerosCount=10;
		array[1][1]=0;array[1][2]=1;array[1][3]=0;array[1][4]=0;array[1][5]=0;
		array[2][1]=1;array[2][2]=0;array[2][3]=0;array[2][4]=0;array[2][5]=1;
		array[3][1]=0;array[3][2]=1;array[3][3]=0;array[3][4]=0;array[3][5]=1;
		array[4][1]=1;array[4][2]=1;array[4][3]=1;array[4][4]=0;array[4][5]=1;
		array[5][1]=0;array[5][2]=0;array[5][3]=1;array[5][4]=0;array[5][5]=0;*/
	
	}
	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getSecond() {
		return second;
	}

	public void setSecond(String second) {
		this.second = second;
	}

	public void setSession(String email,HttpSession http)
	{
		session_details.put(email, http);
	}
	
	public String getOpponent_player() {
		return opponent_player;
	}

	public void setOpponent_player(String opponent_player) {
		this.opponent_player = opponent_player;
	}

	public HttpSession getSession(String email)
	{
		return session_details.get(email);
	}
	
	public void incrementMove_id()
	{
		move_id++;
	}

	public int getMove_id() {
		return move_id;
	}

	public void setMove_id(int move_id) {
		this.move_id = move_id;
	}

	public String getCurrent_player() {
		return current_player;
	}

	public void setCurrent_player(String current_player) {
		this.current_player = current_player;
	}

	public Map<Integer, String> getMove_details() {
		return move_details;
	}

	public void setMove_details(Map<Integer, String> move_details) {
		this.move_details = move_details;
	}

	public int[][] getArray() {
		return array;
	}

	public void setArray(int[][] array) {
		this.array = array;
	}

	public String getPlayer1_email() {
		return player1_email;
	}

	public int getGrid_size_x() {
		return grid_size_x;
	}

	public void setGrid_size_x(int grid_size_x) {
		this.grid_size_x = grid_size_x;
	}

	public int getGrid_size_y() {
		return grid_size_y;
	}

	public void setGrid_size_y(int grid_size_y) {
		this.grid_size_y = grid_size_y;
	}

	public void setPlayer1_email(String player1_email) {
		this.player1_email = player1_email;
	}

	public String getPlayer2_email() {
		return player2_email;
	}

	public void setPlayer2_email(String player2_email) {
		this.player2_email = player2_email;
	}
	
	public void setMove(String s)
	{
		incrementMove_id();
		move_details.put(move_id,s);
	}
	
	public void modifyObject(int row,int arr[],int size)
	{
		int i;
		print();
		System.out.print("Internal"+row+"-");
		for(i=0;i<size;i++)
		{
			System.out.print(arr[i]+",");
			if(array[row][arr[i]]==1)
			{
				array[row][arr[i]]=0;
				nonzerosCount--;
			}
			
		}
		print();
		System.out.println("In Game:"+nonzerosCount);
		if(nonzerosCount<=0)
		{
			gameover=true;
			winner=current_player;
		}
	}
	
	public void swapCurrentUser()
	{
		String temp;
		temp=current_player;
		current_player=opponent_player;
		opponent_player=temp;
		
	}
	
	public void print()
	{
		for(int i=1;i<=grid_size_x;i++)
		{
			for(int j=1;j<=grid_size_y;j++)
			{
				System.out.print(array[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
}
