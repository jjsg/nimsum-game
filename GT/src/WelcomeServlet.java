import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class WelcomeServlet extends HttpServlet{

	ServletContext ctx=null;
	Connection cm=null;
	Statement stmt=null;
	ResultSet rs=null;
	String query=null,level=null;
	String opponent=null;
	String myemail=null;
	int grid_size_x=0,grid_size_y=0;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if(cm==null)
		{
			req.setAttribute("cerror", "Connection Error....");
			req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
		}
		else
		{
			try {
				myemail=(String)req.getSession().getAttribute("email");
				opponent=req.getParameter("opponent");
				stmt=cm.createStatement();
				if(opponent.equals("human"))
				{
						query="SELECT * FROM Players WHERE state=3";
						rs=stmt.executeQuery(query);
						if(rs.next())
						{
							String refemail=rs.getString("email_id");
							query="UPDATE Players SET state=2 WHERE email_id='"+myemail+"' or email_id='"+refemail+"'";
							stmt.execute(query);
							//query="INSERT INTO Games(player1_email,player2_email) VALUES('"+myemail+"','"+refemail+"')";
							//stmt.execute(query);
							ctx=getServletContext();
							HttpSession reference=(HttpSession)ctx.getAttribute(refemail);
							ctx.removeAttribute(refemail);
							Game ps =new Game(myemail,refemail,-1,-1);
							ps.setCurrent_player(myemail);
							ps.setOpponent_player(refemail);
							ps.setSession(myemail, req.getSession());
							ps.setSession(refemail, reference);
							ps.setFirst((String)req.getSession().getAttribute("username"));
							ps.setSecond((String)reference.getAttribute("username"));
							req.setAttribute("message","play");
							req.getSession().setAttribute("gameObject",ps);
							req.getSession().setAttribute("myturn","true");
							reference.setAttribute("gameObject",ps);
							req.getSession().setAttribute("array",ps.array);
							req.getSession().setAttribute("grid_size_x",ps.getGrid_size_x());
							req.getSession().setAttribute("grid_size_y",ps.getGrid_size_y());
							req.getSession().setAttribute("imageselected","5");
							req.getRequestDispatcher("/play.jsp").forward(req, resp);
						}
						else
						{
							req.setAttribute("message","No other player available.<br>Choose your next step:");
							req.getRequestDispatcher("/welcomeback.jsp").forward(req, resp);
						}
				}
				else if(opponent.equals("waiting"))
				{
					query="UPDATE Players SET state=3 WHERE email_id='"+myemail+"'";
					ctx=getServletContext();
					ctx.setAttribute(myemail, req.getSession());
					if(!stmt.execute(query))
					{
						int count=0;
						while(count<21)
						{
							if((req.getSession().getAttribute("gameObject")!=null) && !("".equals(req.getSession().getAttribute("gameObject"))))
								break;
							count++;
							System.out.println(count+"   "+req.getSession().getAttribute("gameObject"));
							Thread.sleep(1000);
						}
						if(count==21)
						{
							System.out.println("wait");
							query="UPDATE Players SET state=0 WHERE email_id='"+myemail+"'";
							if(!stmt.execute(query))
							{
							req.setAttribute("message","Hardluck... Still Wait");
							req.getRequestDispatcher("/welcomeback.jsp").forward(req, resp);
							}
							else
							{
								req.setAttribute("cerror", "Connection Error....");
								req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
								
							}
							
						}
						else
						{
							Game ps=(Game)req.getSession().getAttribute("gameObject");
							req.setAttribute("message","play");
							req.getSession().setAttribute("myturn","false");
							req.getSession().setAttribute("array",ps.getArray());
							req.getSession().setAttribute("grid_size_x",ps.getGrid_size_x());
							req.getSession().setAttribute("grid_size_y",ps.getGrid_size_y());
							req.getSession().setAttribute("firsttime","true");
							req.getSession().setAttribute("imageselected","5");
							req.getRequestDispatcher("/play.jsp").forward(req, resp);
						}
					}
					else
					{
						req.setAttribute("cerror", "Connection Error....");
						req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
					}
							
				}
				else
				{
					query="UPDATE Players SET state=1 WHERE email_id='"+myemail+"'";
					grid_size_x=Integer.parseInt(req.getParameter("grid_size_x"));
					grid_size_y=Integer.parseInt(req.getParameter("grid_size_y"));
					level=req.getParameter("selectt");
					if(!stmt.execute(query))
					{
							Game ps=new Game(myemail,"computer",grid_size_x,grid_size_y);
							ps.setOpponent_player("computer");
							ps.setFirst((String)req.getSession().getAttribute("username"));
							ps.setSecond("computer");
							ps.print();
							req.setAttribute("message","play");
							req.getSession().setAttribute("gameObject",ps);
							req.getSession().setAttribute("myturn","true");
							req.getSession().setAttribute("array",ps.array);
							req.getSession().setAttribute("grid_size_x",ps.getGrid_size_x());
							req.getSession().setAttribute("grid_size_y",ps.getGrid_size_y());
							req.getSession().setAttribute("selectt", level);
							req.getSession().setAttribute("imageselected","5");
							req.getRequestDispatcher("/play.jsp").forward(req, resp);
						
					}
					else
					{
						req.setAttribute("cerror", "Connection Error....");
						req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
					}
				}
			} catch (SQLException e) {
				req.setAttribute("cerror", "Connection Error....");
				req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
				e.printStackTrace();
			} catch (InterruptedException e) {
				req.setAttribute("cerror", "Connection Error....");
				req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
				e.printStackTrace();
			}catch (Exception e) {
				req.setAttribute("cerror", "Connection Error....");
				req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
				e.printStackTrace();
			}
			
			
		}
	}

	@Override
	public void init() throws ServletException {
		cm= new ConnectionManager().getConnection();
	}
	
	

}
