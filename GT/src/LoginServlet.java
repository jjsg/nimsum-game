import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionListener;





public class LoginServlet extends HttpServlet{
	
	ServletContext ctx;
	Connection cm=null;
	Statement stmt=null;
	ResultSet rs=null;
	String query=null;
	String email=null;
	String username=null;
	Map<String, HttpServletRequest> requestmap;
	Map<String,HttpServletResponse> responsemap;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if(cm==null)
		{
			req.setAttribute("cerror", "Connection Error....");
			req.getRequestDispatcher("/login.jsp").forward(req, resp);
		}
		else
		{			
		try {
				email=req.getParameter("email");
				username=req.getParameter("username");
				query="SELECT * FROM Players WHERE email_id='"+email+"'";
				stmt=cm.createStatement();
				rs=stmt.executeQuery(query);
				if(rs.next())
				{
					req.setAttribute("cerror", "Already Registered");
					req.getRequestDispatcher("/login.jsp").forward(req, resp);
				}
				else
				{
					
					query="INSERT INTO Players values('"+email+"','"+username+"',0)";
					System.out.println(query);
					boolean b=stmt.execute(query);
					System.out.println(b);
					if(!b)
					{
						req.getSession().setAttribute("username",username);
						req.getSession().setAttribute("email",email);
						
						req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
					}
					else
					{
						req.setAttribute("cerror", "Connection Error....");
						req.getRequestDispatcher("/login.jsp").forward(req, resp);
					}
					
					
				}
			}
			catch (SQLException e) 
			{
				req.setAttribute("cerror", "Connection Error....");
				req.getRequestDispatcher("/login.jsp").forward(req, resp);
				e.printStackTrace();
			}
			
		}
		
	}

	@Override
	public void init() throws ServletException
	{
		cm=new ConnectionManager().getConnection();	
	}
	
	

}
