import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class PlayServlet extends HttpServlet{

	Game go;
	String move,image,level,forfeit;
	int row;
	int size;
	int arr[]=new int[20];
	String cols[];
	int copy[][];
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	try{
		int i;
		System.out.println("in servlet");
		go=(Game)req.getSession().getAttribute("gameObject");
		if(!"true".equals(req.getSession().getAttribute("firsttime")))
		{
				move=req.getParameter("move");
				image=req.getParameter("imageselected");
				req.getSession().setAttribute("imageselected",image);
				System.out.println(move);
				go.setMove(move); //store move in map
				cols=move.split("-");
				row=Integer.parseInt(cols[0]);
				cols=cols[1].split(",");
				for(i=0;i<cols.length;i++)
				{
					arr[i]=Integer.parseInt(cols[i]);
				}
		}
		else
		{
			System.out.println("giii");
			row=-1;
		}
		if(go.getOpponent_player().equals("computer"))
		{
			go.modifyObject(row, arr, cols.length); // modify the game according to move
			level=(String)req.getSession().getAttribute("selectt");
			if(go.gameover || row==0)
			{
				/*myturn*/
				if(row==0)
				{
					req.getSession().setAttribute("message","Sorry..... You Lost :("); //block this user
				}
				else
				{
					req.getSession().setAttribute("message","Congratzzz!!!! You Won :)"); //block this user
				}
				req.getSession().setAttribute("originalpattern",go.original);
				req.getSession().setAttribute("result",go.getMove_details());
				req.getSession().setAttribute("first",go.getFirst());
				req.getSession().setAttribute("second",go.getSecond());
				req.getSession().removeAttribute("gameObject");
				req.getRequestDispatcher("/result.jsp").forward(req, resp);
			}
			else
			{
				//computer
				go.swapCurrentUser();
				copy = new int [go.getGrid_size_x()+1][go.getGrid_size_y()+1];
				for(i=1;i<=go.getGrid_size_x();i++)
				{
					for(int j=1;j<=go.getGrid_size_y();j++)
					{
						copy[i][j]=go.array[i][j];
					}
				}
				if(level.equals("Difficult"))
					move=new Nim().expertNim(copy);
				else if(level.equals("Easy"))
					move=new Nim().easyNim(copy);
				else
					move=new Nim().mediumNim(copy);
				System.out.println("computer"+move);
				go.setMove(move); //store move in map
	//			go.print();
				cols=move.split("-");
				row=Integer.parseInt(cols[0]);
				cols=cols[1].split(",");
				for(i=0;i<cols.length;i++)
				{
					arr[i]=Integer.parseInt(cols[i]);
				}
				go.modifyObject(row, arr, cols.length); // modify the game according to move
				if(go.gameover)
				{
					/*myturn*/
					req.getSession().setAttribute("message","Sorry..... You Lost :("); //block this user
					req.getSession().setAttribute("originalpattern",go.original);
					req.getSession().setAttribute("result",go.getMove_details());
					req.getSession().setAttribute("first",go.getFirst());
					req.getSession().setAttribute("second",go.getSecond());
					req.getSession().removeAttribute("gameObject");
					req.getRequestDispatcher("/result.jsp").forward(req, resp);
				}
				else
				{
					go.swapCurrentUser();
					//Logic Where computer will play move and store in map
					req.getSession().setAttribute("gameObject", go);
					req.getSession().setAttribute("myturn","true");
					req.getSession().setAttribute("array",go.getArray());
					req.getSession().setAttribute("grid_size_x",go.getGrid_size_x());
					req.getSession().setAttribute("grid_size_y",go.getGrid_size_y());
					try {
						Thread.sleep(4000);
						req.getRequestDispatcher("/play.jsp").forward(req, resp);
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
			
		}
		else
		{
			req.getSession().setAttribute("myturn","false"); //block this user
			if(!"true".equals(req.getSession().getAttribute("firsttime")))
				go.modifyObject(row, arr, cols.length); // modify the game according to move
			if(go.gameover || row==0)
			{
				
				if(row==0)
				{
					req.getSession().setAttribute("message","Sorry..... You Lost :("); //block this user
					go.getSession(go.getOpponent_player()).setAttribute("message","Congratzzz!!!! You Won :)"); //allow other player to continue
				}
				else
				{
					req.getSession().setAttribute("message","Congratzzz!!!! You Won :)"); //block this user
					go.getSession(go.getOpponent_player()).setAttribute("message","Sorry..... You Lost :("); //allow other player to continue
				}
				go.getSession(go.getOpponent_player()).setAttribute("myturn","win"); //allow other player to continue
				go.getSession(go.getOpponent_player()).setAttribute("originalpattern",go.original); //allow other player to continue
				go.getSession(go.getOpponent_player()).setAttribute("result",go.getMove_details()); //allow other player to continue
				go.getSession(go.getOpponent_player()).setAttribute("first",go.getFirst()); //allow other player to continue
				go.getSession(go.getOpponent_player()).setAttribute("second",go.getSecond()); //allow other player to continue
				req.getSession().setAttribute("myturn","win"); //block this user
				req.getSession().setAttribute("originalpattern",go.original);
				req.getSession().setAttribute("result",go.getMove_details());
				req.getSession().setAttribute("first",go.getFirst());
				req.getSession().setAttribute("second",go.getSecond());
				
				req.getSession().removeAttribute("gameObject");
				req.getRequestDispatcher("/result.jsp").forward(req, resp);
			}
			else
			{
				if(!"true".equals(req.getSession().getAttribute("firsttime")))
				{
					go.getSession(go.getOpponent_player()).setAttribute("gameObject",go); //allow other player to continue
					go.getSession(go.getOpponent_player()).setAttribute("array",go.getArray()); //allow other player to continue
					go.getSession(go.getOpponent_player()).setAttribute("grid_size_x",go.getGrid_size_x()); //allow other player to continue
					go.getSession(go.getOpponent_player()).setAttribute("grid_size_y",go.getGrid_size_y()); //allow other player to continue
					go.getSession(go.getOpponent_player()).setAttribute("myturn","true"); //allow other player to continue
					go.swapCurrentUser();
				}
				else
				{
					System.out.println("loop");
					req.getSession().removeAttribute("firsttime");
				}
			boolean flag=true;
				while(((String)req.getSession().getAttribute("myturn")).equals("false"))
					{
					if(req.getSession().getAttribute("quit")!=null)
					{
						if((String)req.getSession().getAttribute("quit")!=null)
						{
							
							Enumeration keys = req.getSession().getAttributeNames();
							while (keys.hasMoreElements())
							{
							  String key = (String)keys.nextElement();
							  if(key.equals("username")||key.equals("email")||key.equals("quit"))
								  continue;
							  req.getSession().removeAttribute(key);
							}
							
							req.getRequestDispatcher("/quit.jsp").forward(req, resp);
							flag=false;
							break;
						}
					}
					}
				if(flag==true){
				if(((String)req.getSession().getAttribute("myturn")).equals("true"))
				{
				req.getRequestDispatcher("/play.jsp").forward(req, resp);
				}
				else
				{
					
					req.getRequestDispatcher("/result.jsp").forward(req, resp);
				}
				
				}
			}
		}
	}catch(Exception e)
	{
		if(req.getSession().getAttribute("gameObject")!=null)
		{
		Game obj=(Game) req.getSession().getAttribute("gameObject");
		if(obj.getOpponent_player()!=null&&obj.getSession(obj.getOpponent_player())!=null)
		{
			System.out.println(req.getSession().getAttribute("username")+":setting of "+	obj.getSession(obj.getOpponent_player()).getAttribute("username"));
		obj.getSession(obj.getOpponent_player()).setAttribute("quit","Sorry, the other player quit the game"); //allow other player to continue
		}
		}
		Enumeration keys = req.getSession().getAttributeNames();
		while (keys.hasMoreElements())
		{
		  String key = (String)keys.nextElement();
		  if(key.equals("username")||key.equals("email"))
			  continue;
		  req.getSession().removeAttribute(key);
		}
		req.getSession().setAttribute("quit","Sorry, the other player quit the game");
		req.getRequestDispatcher("/quit.jsp").forward(req, resp);
		e.printStackTrace();
	}
	}


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("in getttt");
	}

	
	
}
