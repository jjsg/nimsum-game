import java.util.Random;

public class Nim{
	public String expertNim(int array[][]){
		int piles=array.length;
		int sticks[]=new int[piles];
		int win=0;
		String binary,answer=null;
		String sticksBinary[]=new String[piles];
		for(int i=0;i<piles;i++){
			sticks[i]=0;
			for(int j=0;j<array[i].length;j++)
				sticks[i]=sticks[i]+array[i][j];
		}
		int maxlength=Integer.MIN_VALUE;
		for(int i=0;i<piles;i++){
			win=win^sticks[i];
			sticksBinary[i]=Integer.toBinaryString(sticks[i]);
			if(sticksBinary[i].length()>maxlength)
				maxlength=sticksBinary[i].length();
		}
		binary=Integer.toBinaryString(win);
		if(binary.length()>maxlength)
			maxlength=binary.length();
		else{
			int ad=maxlength-binary.length();
			while(ad>0){
				binary="0"+binary;
				ad=ad-1;
			}
		}
		for(int i=0;i<piles;i++){
			if(sticksBinary[i].length()<maxlength){
				int ad=maxlength-sticksBinary[i].length();
				while(ad>0){
					sticksBinary[i]="0"+sticksBinary[i];
					ad=ad-1;
				}
			}
		}
		if(win==0){//No optimal move
			int minimum=Integer.MAX_VALUE;
			int minIndex=0;
			for(int i=0;i<piles;i++){
				if(sticks[i]!=0 && sticks[i]<=minimum){
					minimum=sticks[i];
					minIndex=i;
				}
			}
			sticks[minIndex]=sticks[minIndex]-1;
			answer=Integer.toString(minIndex)+"-";
			for(int y=0;y<array[minIndex].length;y++){
				if(array[minIndex][y]==1){
					array[minIndex][y]=0;
					answer=answer+""+Integer.toString(y);
					break;
				}
			}
		}else{
			int index=0,fromIndex=0,flag=0,opt=0;
			index=binary.indexOf('1', fromIndex);
				for(int i=0;i<piles;i++){
					if(sticksBinary[i].charAt(index)=='1'){
						flag=0;
						while(index!=-1){
							fromIndex=index;
							index=binary.indexOf('1', fromIndex+1);
							if(index!=-1){
								if(sticksBinary[i].charAt(index)=='1')
									continue;
								else{
									flag=1;
									break;
								}
							}
						}
						if(flag==0){//there is optimal move
							fromIndex=0;
							index=binary.indexOf('1', fromIndex);
							while(index!=-1){
								if(index!=sticksBinary[i].length()-1)
									sticksBinary[i]=sticksBinary[i].substring(0, index)+'0'+sticksBinary[i].substring(index+1);
								else
									sticksBinary[i]=sticksBinary[i].substring(0, index)+'0';
								fromIndex=index;
								index=binary.indexOf('1',fromIndex+1);
							}
							int prev=sticks[i];
							sticks[i]=Integer.parseInt(sticksBinary[i],2);
							int diff=prev-sticks[i];
							answer=Integer.toString(i)+"-";
							for(int cr=0;cr<array[i].length;cr++){
								if(array[i][cr]==1){
									array[i][cr]=0;
									diff=diff-1;
									if(diff==0){
										answer=answer+Integer.toString(cr);
										break;
									}else
										answer=answer+Integer.toString(cr)+",";
								}
							}
							opt=1;
							break;
						}
					}
					fromIndex=0;
					index=binary.indexOf('1', fromIndex);
				}
				if(opt==0){//trying for nearest optimal move
					fromIndex=0;
					index=binary.indexOf('1', fromIndex);
					String maxArrayV="0";
					int track=-1;
					for(int i=0;i<piles;i++){
						if(sticksBinary[i].charAt(index)=='1'){
							if(Integer.parseInt(sticksBinary[i], 2)>Integer.parseInt(maxArrayV,2)){
								maxArrayV=sticksBinary[i];
								track=i;
							}
						}
					}
					if(track!=-1){
						int crap=0;
						while(sticks[track]!=0){
							sticks[track]=sticks[track]-1;
							crap=crap+1;
							win=0;
							for(int yk=0;yk<piles;yk++)
								win=win^sticks[yk];
							if(win==0){
								answer=""+track+"-";
								for(int kj=0;kj<array[track].length;kj++){
									if(array[track][kj]==1){
										array[track][kj]=0;
										crap=crap-1;
										if(crap==0){
											answer=answer+""+kj;
											break;
										}
										else
											answer=answer+""+kj+",";
									}
								}
							
							}
						}
					}
				}
		}
		return answer;

	}
	public String mediumNim(int array[][]){
		int piles=array.length;
		int sticks[]=new int[piles];
		int win=0;
		String binary,answer=null;
		String sticksBinary[]=new String[piles];
		for(int i=0;i<piles;i++){
			sticks[i]=0;
			for(int j=0;j<array[i].length;j++)
				sticks[i]=sticks[i]+array[i][j];
		}
		int maxlength=Integer.MIN_VALUE;
		for(int i=0;i<piles;i++){
			win=win^sticks[i];
			sticksBinary[i]=Integer.toBinaryString(sticks[i]);
			if(sticksBinary[i].length()>maxlength)
				maxlength=sticksBinary[i].length();
		}
		binary=Integer.toBinaryString(win);
		if(binary.length()>maxlength)
			maxlength=binary.length();
		else{
			int ad=maxlength-binary.length();
			while(ad>0){
				binary="0"+binary;
				ad=ad-1;
			}
		}
		for(int i=0;i<piles;i++){
			if(sticksBinary[i].length()<maxlength){
				int ad=maxlength-sticksBinary[i].length();
				while(ad>0){
					sticksBinary[i]="0"+sticksBinary[i];
					ad=ad-1;
				}
			}
		}
		if(win==0){//No optimal move, so remove entire minimum row
			int minimum=Integer.MAX_VALUE;
			int minIndex=0;
			for(int i=0;i<piles;i++){
				if(sticks[i]!=0 && sticks[i]<=minimum){
					minimum=sticks[i];
					minIndex=i;
				}
			}
			int temp=sticks[minIndex];
			answer=Integer.toString(minIndex)+"-";
			for(int y=0;y<array[minIndex].length;y++){
				if(array[minIndex][y]==1){
					array[minIndex][y]=0;
					temp=temp-1;
					if(temp>0)
						answer=answer+""+Integer.toString(y)+",";
					else{
						answer=answer+""+Integer.toString(y);
						break;
					}
					
				}
			}
		}else{
			int index=0,fromIndex=0,flag=0,opt=0;
			index=binary.indexOf('1', fromIndex);
				for(int i=0;i<piles;i++){
					if(sticksBinary[i].charAt(index)=='1'){
						flag=0;
						while(index!=-1){
							fromIndex=index;
							index=binary.indexOf('1', fromIndex+1);
							if(index!=-1){
								if(sticksBinary[i].charAt(index)=='1')
									continue;
								else{
									flag=1;
									break;
								}
							}
						}
						if(flag==0){//there is optimal move
							fromIndex=0;
							index=binary.indexOf('1', fromIndex);
							while(index!=-1){
								if(index!=sticksBinary[i].length()-1)
									sticksBinary[i]=sticksBinary[i].substring(0, index)+'0'+sticksBinary[i].substring(index+1);
								else
									sticksBinary[i]=sticksBinary[i].substring(0, index)+'0';
								fromIndex=index;
								index=binary.indexOf('1',fromIndex+1);
							}
							int prev=sticks[i];
							sticks[i]=Integer.parseInt(sticksBinary[i],2);
							int diff=prev-sticks[i];
							answer=Integer.toString(i)+"-";
							for(int cr=0;cr<array[i].length;cr++){
								if(array[i][cr]==1){
									array[i][cr]=0;
									diff=diff-1;
									if(diff==0){
										answer=answer+Integer.toString(cr);
										break;
									}else
										answer=answer+Integer.toString(cr)+",";
								}
							}
							opt=1;
							break;
						}
					}
					fromIndex=0;
					index=binary.indexOf('1', fromIndex);
				}
				if(opt==0){//trying for nearest optimal move
					fromIndex=0;
					index=binary.indexOf('1', fromIndex);
					String maxArrayV="0";
					int track=-1;
					for(int i=0;i<piles;i++){
						if(sticksBinary[i].charAt(index)=='1'){
							if(Integer.parseInt(sticksBinary[i], 2)>Integer.parseInt(maxArrayV,2)){
								maxArrayV=sticksBinary[i];
								track=i;
							}
						}
					}
					if(track!=-1){
						answer=Integer.toString(track)+"-";
						sticks[track]=sticks[track]-1;
						for(int yu=0;yu<array[track].length;yu++){
							if(array[track][yu]==1){
								array[track][yu]=0;
								answer=answer+""+Integer.toString(yu);
								break;
							}
						}
					}
				}
		}
		//System.out.println("XOR value is "+win);
		return answer;
	}

	public String easyNim(int array[][]){
		int piles=array.length;
		System.out.println("Length"+array.length);
		int sticks[]=new int[piles];
		String answer=null;
		Random rg=new Random();
		int row=rg.nextInt(piles);
		System.out.println("Rowwww: "+row);
		for(int i=0;i<piles;i++){
			sticks[i]=0;
			for(int j=0;j<array[i].length;j++)
				sticks[i]=sticks[i]+array[i][j];
		}
		while(sticks[row]==0){
			row=rg.nextInt(piles);
		}
		System.out.println("Rowwww: "+row);
		int stk=rg.nextInt(sticks[row])+1;
		answer=Integer.toString(row)+"-";
		for(int i=0;i<array[row].length;i++){
			if(array[row][i]==1){
				array[row][i]=0;
				stk=stk-1;
				if(stk==0){
					answer=answer+""+Integer.toString(i);
					break;
				}
				else
					answer=answer+""+Integer.toString(i)+",";
			}
		}
		return answer;	
	}
	public static void main(String[] args) {
		int array[][]=new int[][]{new int[]{0,0,0,0,0,0},new int[]{0,0,0,0,0,0},new int[]{0,0,0,0,0,1},new int[]{0,0,0,0,0,1},new int[]{0,0,0,0,0,0},new int[]{0,0,0,0,0,0}};
		Nim obj=new Nim();
		System.out.println(obj.expertNim(array));
		}
}
/*
 0 0 0 0 0 
0 0 0 0 1 
0 0 0 0 1 
0 0 0 0 0 
0 0 0 0 0
 */
