import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class LogoutServlet extends HttpServlet{

	Connection cm=null;
	Statement stmt=null;
	ResultSet rs=null;
	String query=null;
	String email=null;
	
	@Override
	public void init() throws ServletException {
		cm=new ConnectionManager().getConnection();
		
	}

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		try {
			
			String s=req.getParameter("playing");
			if("true".equals(s))
			{
			
				//came from play
				
			//alert another and in polling check if it is set
				if(req.getSession().getAttribute("gameObject")!=null)
				{
				Game obj=(Game) req.getSession().getAttribute("gameObject");
				if(obj.getOpponent_player()!=null&&obj.getSession(obj.getOpponent_player())!=null)
				{
					System.out.println(req.getSession().getAttribute("username")+":setting of "+	obj.getSession(obj.getOpponent_player()).getAttribute("username"));
				obj.getSession(obj.getOpponent_player()).setAttribute("quit","Sorry, the other player quit the game"); //allow other player to continue
				}
				}
				Enumeration keys = req.getSession().getAttributeNames();
				while (keys.hasMoreElements())
				{
				  String key = (String)keys.nextElement();
				  if(key.equals("username")||key.equals("email"))
					  continue;
				  req.getSession().removeAttribute(key);
				}
				req.getSession().setAttribute("quit","You quit the game");
				req.getRequestDispatcher("/quit.jsp").forward(req, resp);
			}
			else
			{
			email=(String)req.getSession().getAttribute("email");
			query="DELETE FROM Players WHERE email_id='"+email+"'";
			stmt=cm.createStatement();
			if(!stmt.execute(query))
			{
				req.getSession().invalidate();
				req.getRequestDispatcher("/login.jsp").forward(req, resp);
			}
			else
			{
				req.setAttribute("cerror", "Connection Error....");
				req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
			}
			}
		} catch (SQLException e) {
			req.setAttribute("cerror", "Connection Error....");
			req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
			e.printStackTrace();
		}
		catch(Exception e)
		{
			req.setAttribute("cerror", "Connection Error....");
			req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
			e.printStackTrace();
		}
		
		}
	

}
