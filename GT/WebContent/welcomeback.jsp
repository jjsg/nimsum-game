<!DOCTYPE html>
<html>
<head>
<title>NimSum</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<link rel="icon" href="css/images/favicon.gif" type="image/x-icon" />
<!--[if IE 6]><link rel="stylesheet" href="css/ie6-style.css" type="text/css" media="all" /><![endif]-->
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/fns.js" type="text/javascript"></script>
</head>
<body>
<%
response.setHeader("Cache-Control","no-cache");
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);
if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
//	out.println("hererrrrrrrr");
	response.setStatus(response.SC_MOVED_TEMPORARILY);
	response.setHeader("Location", "/GT/login.jsp"); 
}
else
{
%>
<!-- Page -->
<div id="page" class="shell">
  <!-- Header -->
  <div id="header">
    <!-- Top Navigation -->
    <div id="top-nav">
      <ul>
        <li class="home"><a href="index.jsp">home</a></li>
        
      </ul>
    </div>
    <!-- / Top Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Logo -->
    <div id="logo">
     
    </div>
    <!-- / Logo -->
    <!-- Main Navigation -->
    <div id="main-nav">
      <div class="bg-right">
        <div class="bg-left">
          

        </div>
      </div>
    </div>
    <!-- / Main Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Sort Navigation -->
    <div id="sort-nav">
      <div class="bg-right">
        <div class="bg-left">
          <div class="cl">&nbsp;</div>
          <ul>
              <li><a href="index.jsp">Home</a><span class="sep">&nbsp;</span></li>      
            <li><a onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a><span class="sep">&nbsp;</span></li>
            <li><a href="info.jsp">Know Nim!</a><span class="sep">&nbsp;</span></li>
            <li><a href="tutorial.jsp">Tutorial</a><span class="sep">&nbsp;</span></li>
           <li><a href="https://www.facebook.com/NimSumGame" target="_blank">Follow us on FB</a><span class="sep">&nbsp;</span></li>
           <li><a href="aboutus.jsp">Know Us!</a><span class="sep">&nbsp;</span></li>
          </ul>
          <div class="cl">&nbsp;</div>
        </div>
      </div>
    </div>
    <!-- / Sort Navigation -->
  </div>
  <!-- / Header -->

  <!-- Main -->
  <div id="main">
    <div id="main-bot">
      <div class="cl">&nbsp;</div>
      <!-- Content -->
      <div id="content">
     <div style="background: #C9CCD3;border: solid 10px #313131; border-radius: 5px;">
          <!-- a id="back" href="loginclear.jsp" onclick="return confirm('Are you sure you want to logout')" >Logout</a-->       
                <br/><br/>
        
       
    <p style="padding: 5px; color:  #e05f1d;font-size:20px; text-align: center;">   
<%=request.getAttribute("message") %> <br><br><br>
        <button class="vsclass" id="vshuman" onclick="setHuman()" onmouseover="hidegrid()">Wait!!!</button>&nbsp;&nbsp;
          <button class="vsclass" id="vscomputer" onclick="setComputer()" onmouseover="showgrid()">Play vs Computer</button>
    <br>
    <progress id="progressbar" style="margin-left: 170px; display:none;" value="0" max="30">abcdef</progress>
         </p><br>
        <script>
        function setHuman()
            {
                 x=document.getElementById("opponent");
                
                    x.value="waiting";
            
                 x=document.getElementById("gridform");
               x.action="welcome";
               x.submit();
                //document.write('<input type="text"/>');
                document.getElementById("progressbar").style.display="block";
                document.getElementById("vshuman").style.display="none";
                document.getElementById("vscomputer").style.display="none";
                setInterval(function(){x=document.getElementById("progressbar");y=x.value;
                y=y+1;
                x.value=y;}, 1000);
                
            }
            function progressupdate()
            {
                x=document.getElementById("progressbar");
                x.value=x.value+1;
                //alert("hi");
            }
        function setComputer()
            {x=document.getElementById("opponent");
                    x.value="computer";
            }
        function showgrid()
            {x=document.getElementById("opponent");
                    x.value="computer";
                x=document.getElementById("gridsize");
               x.style.display="block";
              x=document.getElementById("vscomputer");
             x.style.fontSize="30px";
             x.style.backgroundColor="#e05f1d";
             x.style.color="black";
            }
        function hidegrid()
            {  x=document.getElementById("gridsize");
               x.style.display="none";
              x=document.getElementById("vscomputer");
             x.style.fontSize="20px";
             x.style.backgroundColor="#E4E4E4";
             x.style.color="#e05f1d";
               
            }
        function setcomputeraction(){
            x=document.getElementById("gridform");
   		if(validate())
           {
             	 x.action="welcome";
		               x.submit();
           }
           else
           {
           	document.getElementById("cerror").innerHTML=msg;
           }
       }
       
       function validate()
       {
				 var x=document.getElementsByName("grid_size_x")[0].value;
				var y=document.getElementsByName("grid_size_y")[0].value;
				if((x<3 || x>10) && (y<3 || y>10))
					msg="Invalid x and y";
				else if(y<3 || y>10)
					msg="Invalid y";
				else if(x<3 || x>10)
					msg="Invalid x";
				else
					return true;
				return false;
       }
       function selectf()
       {
       	  var x = document.getElementById("selectlevel").selectedIndex;
       	    var y = document.getElementById("selectlevel").options;
       	   
       	document.getElementById("selectt").value=y[x].text;
       }
        </script>
        
        <div id="gridsize" style="display: none">
<form id="gridform" method="POST">
    <h3>Select the grid size.</h3><h4>Note: the arrangement of the objects in the grid will be random.</h4>
		

 <table>
 	    <tr><td colspan=2>
 	    <label style="color:red" id="cerror">

 </label>
 </td></tr>
		<tr><td>On X-axis: </td>
		<td>
		<input  type="number"  min="3" max="10" name="grid_size_x" style="height: 30px; width: 200px; font-size: 15px;" required placeholder="value between 3 to 10" /><span class="star" style="color:red">*</span><br/></td>
		</tr>
		<tr>
		<td>On Y-axis: </td>
		<td><input type="number"  min="3" max="10" name="grid_size_y" style="height: 30px; width: 200px; font-size: 15px;" placeholder="value between 3 to 10" required><span class="star" style="color:red">*</span>
		<br/></td>
		</tr>
		<tr>
		<td>Computer Level: </td>
		<td>
		<select id="selectlevel" onchange="selectf()" name="level">
		<option value="1">Easy</option>
		<option selected value="2">Medium</option>
		<option value="3">Difficult</option>
		</select>
		</td>
		</tr>
		<tr><td></td>
		<td><input type="button" id="csubmit" onclick="setcomputeraction()" value="Play"/></td>
		</tr>
	
</table>
    
<input type="text" hidden="hidden"  id="opponent" name="opponent"/>	
<input type="text"  hidden="hidden" id="selectt" value="Medium" name="selectt"/>	
	
	</form>


</div><!-- gridesize -->
          </div><!-- div next to content -->
     
      </div>
      <!-- / Content -->
	</div>    
	<!-- main-bot -->
    <div id="sidebar">
     
   
      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt">
              <h3>Know Nim!</h3>
            </div>
          </div>
          <div class="image-articles articles">
            <div class="cl">&nbsp;</div>
            <div class="article">
              <div class="cl">&nbsp;</div>
              
              <div class="cnt" style="float: left;">
              
                <p style="text-align: justify;">Your winning chance depends on the roll of a die or the cards you've been dealt. But there are other games that are only about strategy: if you play cleverly, you're guaranteed to win...</p>
              </div>
              <div class="cl">&nbsp;</div>
            </div>
            <div class="article">
             
            <a href="info.jsp" class="view-all">view more...</a>
            <div class="cl">&nbsp;</div>
          </div>
        </div>
      </div>
      </div>
      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt">
              <h3>Tutorial</h3>
            </div>
          </div>
          <div class="image-articles articles">
            
           
            <div class="cl">&nbsp;</div>
            <div class="article">
              <div class="cl">&nbsp;</div>
              
              <div class="cnt" style="float: left;">
             <div>
<iframe width="210" height="250" src="http://www.youtube.com/embed/cafwUNrg73M" frameborder="0" allowfullscreen></iframe>
</div>

               
              </div>
              <div class="cl">&nbsp;</div>
            </div>
         
            <div class="cl">&nbsp;</div>
          </div>
          <!-- image article -->
        </div>
      </div>
      
    </div>
    
    <!-- / Sidebar -->
    <div class="cl">&nbsp;</div>
    <!-- Footer -->
    <div id="footer">
      <div class="navs">
        <div class="navs-bot">
          <div class="cl">&nbsp;</div>
          <ul>
    <li><a href="index.jsp">Home</a><span class="sep">&nbsp;</span></li>      
             <li><a onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a><span class="sep">&nbsp;</span></li>
            <li><a href="info.jsp">Know Nim!</a><span class="sep">&nbsp;</span></li>
      
            <li><a href="tutorial.jsp">Tutorial</a><span class="sep">&nbsp;</span></li>
           <li><a href="https://www.facebook.com/NimSumGame" target="_blank">Follow us on FB</a><span class="sep">&nbsp;</span></li>
           <li><a href="aboutus.jsp">Know Us!</a><span class="sep">&nbsp;</span></li>
          </ul>
         
          <div class="cl">&nbsp;</div>
        </div>
      </div>
      <p class="copy" style="color: #e05f1d;">&copy;Designed By: NimSum PG2 Team</p>
    </div>
    <!-- / Footer -->
  </div>

<!-- / Main -->
</div>
<!-- / Page -->
<%
}
%>
</body>
</html>
