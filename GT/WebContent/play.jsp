<!DOCTYPE html>
<html>
<head>
<title>NimSum</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<link rel="icon" href="css/images/favicon.gif" type="image/x-icon" />
<!--[if IE 6]><link rel="stylesheet" href="css/ie6-style.css" type="text/css" media="all" /><![endif]-->
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/fns.js" type="text/javascript"></script>
</head>
<body>
<%
response.setHeader("Cache-Control","no-cache");
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);
if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
//	out.println("hererrrrrrrr");
	response.setStatus(response.SC_MOVED_TEMPORARILY);
	response.setHeader("Location", "/GT/login.jsp"); 
}
else
{	
%>
<!-- Page -->
<div id="page" class="shell">
  <!-- Header -->
  <div id="header">
    <!-- Top Navigation -->
    <div id="top-nav">
       
      
           
    </div>
    <!-- / Top Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Logo -->
    <div id="logo">
     
    </div>
    <!-- / Logo -->
    <!-- Main Navigation -->
    <div id="main-nav">
      <div class="bg-right">
        <div class="bg-left">
          

        </div>
      </div>
    </div>
    <!-- / Main Navigation -->
    <div class="cl">&nbsp;</div>

  </div>
  <!-- / Header -->

  <!-- Main -->
  <div id="main">
    <div id="main-bot">
      <div id="playdivenable" style="width:905px; border: solid 5px #e05f1d; color: black;background: #a0a0a0;" >
    <iframe width="905" src="/GT/playiframe.jsp" frameborder="0" allowfullscreen scrolling="no" onload='javascript:resizeIframe(this);' ></iframe>

		</div>
    
      <script type="text/javascript">
      function resizeIframe(obj) {
          obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        }
      </script>
	</div>    
	<!-- main-bot -->
    
    <div class="cl">&nbsp;</div>
    <!-- Footer -->
    <div id="footer">
      <div class="navs">
        <div class="navs-bot">
          <div class="cl">&nbsp;</div>
         
         
          <div class="cl">&nbsp;</div>
        </div>
      </div>
      <p class="copy" style="color: #e05f1d;">&copy;Designed By: NimSum PG2 Team</p>
    </div>
    <!-- / Footer -->
  </div>

<!-- / Main -->
</div>
<!-- / Page -->

<%
//while(((String)session.getAttribute("myturn")).equals("false"));

/*if((((String)session.getAttribute("myturn")).equals("win")))
{
	response.setStatus(response.SC_MOVED_TEMPORARILY);
	response.setHeader("Location", "/GT/win.jsp"); 
}
else if((((String)session.getAttribute("myturn")).equals("true")))
{
	out.println("<script>document.getElementById(\"playdivenable\").style.pointerEvents=\"auto\" </script>");
}
*/
}
%>
</body>
</html>
