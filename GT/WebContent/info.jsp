<!DOCTYPE html>
<html>
<head>
<title>NimSum</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<link rel="icon" href="css/images/favicon.gif" type="image/x-icon" />
<!--[if IE 6]><link rel="stylesheet" href="css/ie6-style.css" type="text/css" media="all" /><![endif]-->
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/fns.js" type="text/javascript"></script>
</head>
<body>
<!-- Page -->
<%
response.setHeader("Cache-Control","no-cache");
response.setHeader("Cache-Control","no-store");
response.setHeader("Pragma","no-cache");
response.setDateHeader ("Expires", 0);%>
<div id="page" class="shell">
  <!-- Header -->
  <div id="header">
    <!-- Top Navigation -->
    <div id="top-nav">
      <ul>
            <li class="home"><a href="index.jsp">home</a></li><!--------------------TODO session clear on load------>
      </ul>
    </div>
    <!-- / Top Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Logo -->
    <div id="logo">
     
    </div>
    <!-- / Logo -->
    <!-- Main Navigation -->
    <div id="main-nav">
      <div class="bg-right">
        <div class="bg-left">
          
        </div>
      </div>
    </div>
    <!-- / Main Navigation -->
    <div class="cl">&nbsp;</div>
    <!-- Sort Navigation -->
    <div id="sort-nav">
      <div class="bg-right">
        <div class="bg-left">
          <div class="cl">&nbsp;</div>
          <ul>
                      <%  if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
%>
 <li><a href="index.jsp">Home</a><span class="sep">&nbsp;</span></li>
    <li><a href="login.jsp">Log In</a><span class="sep">&nbsp;</span></li>
<% 	 
}
else
{
%> 
 <li><a href="welcome.jsp">Welcome</a><span class="sep">&nbsp;</span></li>
   <li><a  onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a><span class="sep">&nbsp;</span></li>
<% 	 
	
}
 %>            <li><a href="info.jsp">Know Nim!</a><span class="sep">&nbsp;</span></li>
      
            <li><a href="tutorial.jsp">Tutorial</a><span class="sep">&nbsp;</span></li>
           <li><a href="https://www.facebook.com/NimSumGame" target="_blank">Follow us on FB</a><span class="sep">&nbsp;</span></li>
           <li><a href="aboutus.jsp">Know Us!</a><span class="sep">&nbsp;</span></li>
          </ul>
          <div class="cl">&nbsp;</div>
        </div>
      </div>
    </div>
    <!-- / Sort Navigation -->
  </div>
  <!-- / Header -->

  <!-- Main -->
  <div id="main">
    <div id="main-bot">
      <div class="cl">&nbsp;</div>
      <!-- Content -->
      <div id="content">
        <div class="block">
          <div class="block-bot">
            
        </div>
            <div style="background: #C9CCD3;border: solid 10px #313131; border-radius: 5px;">
                <div id="animated-div">Nim !</div>
        
                
                <p style="padding: 5px; color:  #e05f1d;font-size:20px; text-align: center; "><i>Luck vs Strategy</i></p>
             <p style="padding: 45px; text-align:justify; color:  black;font-size:15px; ">    
             In some games, your winning chance depends on the roll of a die or the cards you've been dealt. But there are other games that are only about strategy: if you play cleverly, you're guaranteed to win. 
             <br><br>NimSum is a Combinatorial game with impartial moves. The game play mode that we are following is Normal(i.e. one who picks the last wins).
             <br> <br>The traditional game of Nim is played with a number of coins arranged in heaps: the number of coins and heaps is up to you. There are two players. When it's a player's move he or she can take any number of coins from a single heap. They have to take at least one coin, though, and they can't take coins from more than one heap. The winner is the player who makes the last move, so there are no coins left after that move.
<br>
                 <img src="css/images/example.jpg"/ style="margin-left: 100px; margin-top: 30px;" width="350px" height="300px" draggable="auto">
                 <label style="color: red; font-size: 10px; margin-left: 120px;">A game of Nim starting with heaps of sizes 3, 4 and 5. Player A wins.</label>
</p>
                

</div>

      </div>


     
      </div>
    </div>

    <!-- / Content -->
   
        
    
    <div id="sidebar">
     
  
      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt">
              <h3>Know Nim!</h3>
            </div>
          </div>
          <div class="image-articles articles">
            <div class="cl">&nbsp;</div>
            <div class="article">
              <div class="cl">&nbsp;</div>
              
              <div class="cnt" style="float: left;">
              
                <p style="text-align: justify;">Your winning chance depends on the roll of a die or the cards you've been dealt. But there are other games that are only about strategy: if you play cleverly, you're guaranteed to win...</p>
              </div>
              <div class="cl">&nbsp;</div>
            </div>
            <div class="article">
             
            <a href="info.jsp" class="view-all">view more...</a>
            <div class="cl">&nbsp;</div>
          </div>
        </div>
      </div>
      <div class="block">
        <div class="block-bot">
          <div class="head">
            <div class="head-cnt">
              <h3>Tutorial</h3>
            </div>
          </div>
          <div class="image-articles articles">
            
           
            <div class="cl">&nbsp;</div>
            <div class="article">
              <div class="cl">&nbsp;</div>
              
              <div class="cnt" style="float: left;">
             <div>
<iframe width="210" height="250" src="http://www.youtube.com/embed/cafwUNrg73M" frameborder="0" allowfullscreen></iframe>
</div>

               
              </div>

              <div class="cl">&nbsp;</div>
            </div>
         
            <div class="cl">&nbsp;</div>
          </div>
        </div>
      </div>
      
    </div></div></div>
    <!-- / Sidebar -->
    <div class="cl">&nbsp;</div>
          
    <!-- Footer -->
    <div id="footer">
      <div class="navs">
        <div class="navs-bot">
          <div class="cl">&nbsp;</div>
          <ul>
             <%  if((session.getAttribute("username")==null) || "".equals(session.getAttribute("username")))
{
%>
 <li><a href="index.jsp">Home</a><span class="sep">&nbsp;</span></li>
    <li><a href="login.jsp">Log In</a><span class="sep">&nbsp;</span></li>
<% 	 
}
else
{
%> 
 <li><a href="welcome.jsp">Welcome</a><span class="sep">&nbsp;</span></li>
   <li><a  onclick="return confirm('Are you sure you want to logout')" href="logout">Log Out</a><span class="sep">&nbsp;</span></li>
<% 	 
	
}
 %>
            <li><a href="info.jsp">Know Nim!</a><span class="sep">&nbsp;</span></li>
      
            <li><a href="tutorial.jsp">Tutorial</a><span class="sep">&nbsp;</span></li>
           <li><a href="https://www.facebook.com/NimSumGame" target="_blank">Follow us on FB</a><span class="sep">&nbsp;</span></li>
           <li><a href="aboutus.jsp">Know Us!</a><span class="sep">&nbsp;</span></li>
          </ul>
         
          <div class="cl">&nbsp;</div>
        </div>
      </div>
      <p class="copy" style="color: #e05f1d;">&copy;Designed By: NimSum PG2 Team</p>
    </div>
    <!-- / Footer -->
  </div>
</div>
<!-- / Main -->
</div>
<!-- / Page -->

</body>
</html>

